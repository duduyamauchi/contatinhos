package com.example.contatinhos

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.contatinho_item_lista.view.*

class ContatinhoAdapter(val context: Context, val contatinhos: List<Contatinho>)
    : RecyclerView.Adapter<ContatinhoAdapter.ViewHolder>() {

    var ClickListener : ((index:Int) -> Unit)? = null
    var cliqueLongoListener: ((index:Int) -> Boolean)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.contatinho_item_lista, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return contatinhos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(context, contatinhos[position], ClickListener, cliqueLongoListener)
    }

    fun setOnItemClickListener(clique : ((index:Int) -> Unit)){
        this.ClickListener = clique
    }

    fun configuraCliqueLongo(cliqueLongo: ((index:Int) -> Boolean)?){
        this.cliqueLongoListener = cliqueLongo
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(context: Context,
                     contatinho: Contatinho,
                     clique : ((index:Int) -> Unit)?,
                     cliqueLongoListener: ((index:Int) -> Boolean)?) {
            itemView.tvNome.text = contatinho.nome
            itemView.tvTelefone.text = contatinho.telefone

            val thumbnail = Glide.with(context)
                .load(R.drawable.ic_person)
                .apply(RequestOptions.circleCropTransform())

            Glide.with(context)
                .load(contatinho.caminhoFoto)
                .thumbnail(thumbnail)
                .centerCrop()
                .apply(RequestOptions.circleCropTransform())
                .into(itemView.imgFoto)

            if (clique != null) {
                itemView.setOnClickListener() {
                    clique.invoke(adapterPosition)
//                val editaContatinho = Intent(context, CadastraContatinhoActivity::class.java)
//                editaContatinho.putExtra(CadastraContatinhoActivity.CONTATINHO, contatinho)
//                context.startActivity(editaContatinho)

                }
            }

            if (cliqueLongoListener != null){
                itemView.setOnLongClickListener(){
                    cliqueLongoListener.invoke(adapterPosition)
                }

            }
        }



    }
}