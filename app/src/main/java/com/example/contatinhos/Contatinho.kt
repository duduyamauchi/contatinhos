package com.example.contatinhos

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Contatinho (var nome: String,
                       var telefone: String,
                       var email: String? = null,
                       var endereco: String? = null,
                       @ColumnInfo(name = "caminho_foto")
                       var caminhoFoto:String? = null,
                       @PrimaryKey(autoGenerate = true)
                       var id: Int = 0) : Serializable