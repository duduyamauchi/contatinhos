package com.example.contatinhos

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File

class CadastraContatinhoActivity : AppCompatActivity() {

    companion object{

        public const val CONTATINHO: String = "Contatinho"
        private const val REQUEST_PERMISSOES : Int = 3
        private const val REQUEST_CAMERA: Int = 10
    }

    var caminhoFoto:String? = null
    var caminhoFotoAceita : String? = null
    var contatinho: Contatinho? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        contatinho = intent.getSerializableExtra(CONTATINHO) as Contatinho?

        if (contatinho != null){
            carregaDados()
        }

        btnFoto.setOnClickListener(){
            // Toast.makeText(this, "Tirar foto", Toast.LENGTH_SHORT).show()
            tirarFoto()


        }

        imgTelefone.setOnClickListener(){ view ->
            telefonaOuEnviaMensagem(view)

        }



        imgEmail.setOnClickListener(){
            enviaEmail()
        }

        imgEndereco.setOnClickListener(){
            mostraNoMapa()


        }
    }

    private fun carregaDados() {
        edtNome.setText(contatinho?.nome)
        edtTelefone.setText(contatinho?.telefone)
        edtEmail.setText(contatinho?.email)
        edtEndereco.setText(contatinho?.endereco)

        Glide.with(this)
            .load(contatinho?.caminhoFoto)
            .placeholder(R.drawable.ic_person)
            .centerCrop()
            .into(imgFoto)

        caminhoFotoAceita = contatinho?.caminhoFoto


    }

    private fun tirarFoto() {
        val tirarFoto = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if (tirarFoto.resolveActivity(packageManager) != null) {
            val arquivoFoto = montaArquivoFoto()
            val uriFoto = FileProvider.getUriForFile(this,"${BuildConfig.APPLICATION_ID}.fileprovider", arquivoFoto)
            tirarFoto.putExtra(MediaStore.EXTRA_OUTPUT, uriFoto)
            startActivityForResult(tirarFoto, REQUEST_CAMERA)
        } else {
            Toast.makeText(this, "Impossivel tirar foto", Toast.LENGTH_SHORT).show()
        }
    }

    private fun montaArquivoFoto():File {
        val nomeArquivo = "${System.currentTimeMillis().toString()}"
        val diretorioArquivo = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val arquivoFoto = File.createTempFile(nomeArquivo, ".jpg", diretorioArquivo)

        caminhoFoto = arquivoFoto.absolutePath

        return arquivoFoto

    }

    private fun telefonaOuEnviaMensagem(view: View?) {
        val popup = PopupMenu(this, view)
        popup.inflate(R.menu.menu_telefona_ou_mensagem)

        popup.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.menuMensagem -> enviaMensagem()
                R.id.menuTelefona -> efetuarLigacao()
                else -> false
            }
        }

        popup.show()
    }

    private fun mostraNoMapa() {
        val mostrarNoMapa = Intent(Intent.ACTION_VIEW)
        mostrarNoMapa.data = Uri.parse("geo:0,0?q=${edtEndereco.text} ")

        if (mostrarNoMapa.resolveActivity(packageManager) != null)
            startActivity(mostrarNoMapa)
        else
            Toast.makeText(this, "Impossivel mostrar no mapa", Toast.LENGTH_SHORT).show()
    }

    private fun enviaEmail() {
        val enviarEmail = Intent(Intent.ACTION_VIEW)
        enviarEmail.data = Uri.parse("mailto:${edtEmail.text} ")
        enviarEmail.putExtra(Intent.EXTRA_SUBJECT, "Oi sumida")

        if (enviarEmail.resolveActivity(packageManager) != null)
            startActivity(enviarEmail)
        else
            Toast.makeText(this, "Impossivel enviar e-mail", Toast.LENGTH_SHORT).show()
    }

    private fun enviaMensagem() : Boolean {
        val enviarMensagem = Intent(Intent.ACTION_VIEW)
        enviarMensagem.data = Uri.parse("sms:${edtTelefone.text} ")
        enviarMensagem.putExtra("sms_body", "Oi sumida")

        if (enviarMensagem.resolveActivity(packageManager) != null) {
            startActivity(enviarMensagem)
            return true
        } else {
            Toast.makeText(this, "Impossivel enviar mensagem", Toast.LENGTH_SHORT).show()
            return false
        }
    }

    private fun efetuarLigacao(): Boolean {
        val efetuarLigacao = Intent(Intent.ACTION_CALL, Uri.parse("tel: ${edtTelefone.text}"))
        if (efetuarLigacao.resolveActivity(packageManager) != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), REQUEST_PERMISSOES)
                    return false
                } else {
                    startActivity(efetuarLigacao)
                    return true
                }
            } else {
                startActivity(efetuarLigacao)
                return true
            }
        } else {
            Toast.makeText(this, "Impossivel fazer ligação", Toast.LENGTH_SHORT).show()
            return false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_cadastro, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menuSalvar -> salvaContatinho()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_PERMISSOES && (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(this, "Necessaria permissao para fazer ligação", Toast.LENGTH_SHORT).show()

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {




            Glide.with(this)
                .load(caminhoFoto)
                .placeholder(R.drawable.ic_person)
                .centerCrop()
                .into(imgFoto)

            caminhoFotoAceita = caminhoFoto

//            val arquivoFoto = File(caminhoFoto)
//            if (arquivoFoto.exists()){
//              val bitmap = BitmapFactory.decodeFile(arquivoFoto.absolutePath)
//                imgFoto.setImageBitmap(bitmap)
//            }
//
//        }
        }
    }

    private fun salvaContatinho() {

        if (edtNome.text.isEmpty()){
            edtNome.requestFocus()
            edtNome.setError(getString(R.string.campo_obrigatorio))
            return
        }

        if (edtTelefone.text.isEmpty()){
            edtTelefone.requestFocus()
            edtTelefone.setError(getString(R.string.campo_obrigatorio))
            return
        }

        if (contatinho == null) {
                contatinho = Contatinho(
                edtNome.text.toString(),
                edtTelefone.text.toString(),
                edtEmail.text.toString(),
                edtEndereco.text.toString(),
                caminhoFotoAceita
            )
        }else{
            contatinho?.nome = edtNome.text.toString()
            contatinho?.telefone = edtTelefone.text.toString()
            contatinho?.endereco = edtEndereco.text.toString()
            contatinho?.email = edtEmail.text.toString()
            contatinho?.caminhoFoto = caminhoFotoAceita
        }

        //Toast.makeText(this, contatinho.toString(), Toast.LENGTH_LONG).show()

        val contatinhoDao: ContatinhoDao? = AppDatabase.getInstance(this)?.contatinhoDao()
        doAsync {
            contatinhoDao?.insert(contatinho!!)
            uiThread {
                finish()
            }
        }

    }
}
